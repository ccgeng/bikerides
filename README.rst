==================
Bikeride Challenge
==================

    :Author: CGeng



1 Purpose
---------

This package collects my attempt at the bikeride / weather challenge.

The approach is submitted in the form of sphinx documentation that is prebuilt in the directory named **build** relative to this README, with
****build/html/index.html**** as the natural entry point.
