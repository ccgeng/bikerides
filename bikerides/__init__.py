"""Helper lib to convert AISoundlab data to audformat compatible database.
"""

# from aisoundlabtools import processors
# from aisoundlabtools import pipelines
# import aisoundlabtools.core.data

import bikerides.core.utils
import bikerides.core.visualcrossing
import bikerides.tripingest

# Disencourage from aisoundlabtools import *
__all__ = []


# Dynamically get the version of the installed module
try:
    import pkg_resources

    __version__ = pkg_resources.get_distribution(__name__).version
except Exception:  # pragma: no cover
    pkg_resources = None  # pragma: no cover
finally:
    del pkg_resources
