import functools
import math
import os
import time
from pathlib import Path

import numpy as np
import oyaml as yaml
import pandas as pd


def load_yaml(config_file):

    if not os.path.exists(config_file):
        raise FileNotFoundError()
    if os.path.exists(config_file):
        with open(config_file, 'r') as cf:
            config = yaml.load(cf, Loader=yaml.BaseLoader)

    return config


def save_yaml(path, data, **kwargs):

    with open(path, 'w') as f:
        yaml.dump(data, f, **kwargs)


def get_project_root() -> Path:
    # return Path(__file__).parent.parent
    return Path(os.path.dirname(os.path.abspath(__file__))).parent.parent


def convert_size(size_bytes: int) -> str:
    """convert bytes into human readable format like B|kB|MB ...

    Args:
        size_bytes: size of file to be converted
    Returns:
        size formatted in a human-readable way
    """
    if size_bytes == 0:
        return '0B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return '%s %s' % (s, size_name[i])


def timer(some_function):
    """decorator to measure timing."""

    @functools.wraps(some_function)
    def wrapper(*args):
        """ "Decorator inner function."""
        t1 = time.time()
        result = some_function(*args)
        t2 = time.time()
        print('Time elapsed[s]: ' + str(np.round((t2 - t1), 2)) + '\n')
        return result

    return wrapper
