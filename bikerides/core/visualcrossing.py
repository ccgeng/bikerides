"""visual crossing api related stuff."""
import os
import sqlite3
import urllib
from unicodedata import bidirectional

import click
import pandas as pd
import requests
from numpy import fft

import bikerides
from bikerides.utils import get_project_root
from bikerides.utils import save_yaml

VISUAL_CROSSING_API_KEY = os.getenv('VISUAL_CROSSING_API_KEY')


def get_visual_crossing_request_url(
    API_KEY,
    location='New York',
    start_date='2013-07-01',
    end_date='2014-02-28',
    unit_group='metric',
    include_interval='hours,days',
    content_type='json',
):
    """Assemble HTTP Request URL to retrieve weather data from Visual Crossing Api.

    Args:
        API_KEY: VC API Key
        include_interval: hours|hours,days|days
        unit_group: 'us'|'metric'|'uk'


    unitgroup: 'metric'implies C° and km


    Quoting and unquoting strings:

        display(quote_plus(project))
        display(quote("New York"))
        urllib.parse.unquote_plus("New%20York")


    Dates and Times in the API:
    www.visualcrossing.com/resources/documentation/weather-api/date-and-times-in-the-weather-api/:

    If you would prefer times
    to be directly related to the standard UNIX epoch of 1st January 1970 UTC (GMT) time,
    the API parameter ‘useEpochSeconds=true’ will switch the output
    to include seconds since midnight 1st January 1970 UTC also include a
    ‘datetimeOffset’ property for expected time zone
    and daylight savings offsets in hours.
    """
    if not API_KEY:
        return ValueError('Not API key provided.')

    API_URL = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/'  # noqa: E501
    location = urllib.parse.quote(location)
    unit_group = f'?unitGroup={unit_group}'
    include = '&include='
    include = include + urllib.parse.quote(include_interval)

    key = f'&key={API_KEY}'
    content_type = f'&contentType={content_type}'

    result = f'{API_URL}{location}/{start_date}/{end_date}{unit_group}{include}{key}{content_type}'  # noqa: E501
    return result


def weather_file_name(
    start_date,
    end_date,
    include_interval='hours,days',
    useEpochSeconds=False,
):
    """Assemble download Filename

    Probably not the best scheme as `include_interval_str` values have unequal str len.
    """

    epoch_fpart = {  # noqa: F841
        False: 'no_epochs_seconds',
        True: 'with_epochs_seconds',
    }
    include_interval_str = include_interval.replace(',', '_')

    fname = f'{include_interval_str}_{start_date}_{end_date}_{useEpochSeconds}.yml'
    return fname


def generator_for_day_hour_based_data(
    json_data: dict,
    day_fields=None,
    hour_fields=None,
):
    """Generator through weather data from Visual Crossing using days,hours query.

    Args:
        day_fields: fields to return from the day sub dicts. If None, nothing is returned
        hour_fields: fields to return from the hour sub dicts.
        if None, ALL fields are returned


    To avoid name clashing, [d] is appended to each day_field

    hour_fields will return all information when not specified.
    """

    if hour_fields is None:
        hour_fields = [x for x in json_data['days'][0]['hours'][0].keys()]

    if day_fields is not None:
        day_field_names = [x + '[d]' for x in day_fields]
    else:
        day_field_names = []

    for day in json_data['days']:
        day_data = []
        if day_fields is not None:
            day_data.extend([day[x] for x in day_fields])
        for hour in day['hours']:
            hour_data = [hour[x] for x in hour_fields]
            keys = day_field_names + hour_fields
            values = day_data + hour_data
            yield dict(zip(keys, values))


def get_df_weather(vc_api_data: dict, day_fields: list = None, hour_fields: list = None):
    """get the weather for data yielded from the generator for hourly based based data.

    Args:
        vc_api_data: Visual Crossing Api Data obtained using `hours,days` requst
        day_fields: list of attributes to obtain from days level of the dict tree.
        hoor_fields: list of attributes to obtain from hour level of dict tree.

    Notes on formatting:
        tzoffset: time zone correction
        This is converted to pandas datetime object. It added to datetimeEpoch to
        calculate the local time but left as UTC when returned.
        The hourly weather is also returned, but as 'date_hh'


    tzoffset Comes from "day level" data of the dict tree obtained form API.
    There is waws specified as an integer that needs to eb added to local datetime.
    """

    df_weather = (
        pd.DataFrame.from_records(
            generator_for_day_hour_based_data(
                vc_api_data,
                day_fields=day_fields,
                hour_fields=hour_fields,
            )
        )
        .infer_objects()
        .convert_dtypes()
    )

    # comvert if day time info has been extracted
    if 'datetimeEpoch[d]' in df_weather.columns:
        df_weather['datetimeEpoch[d]'] = pd.to_datetime(
            df_weather['datetimeEpoch[d]'], unit='s'
        )

    df_weather['datetimeEpoch'] = pd.to_datetime(df_weather['datetimeEpoch'], unit='s')
    df_weather['timezone'] = vc_api_data['timezone']
    df_weather['tzoffset'] = pd.to_timedelta(
        float(vc_api_data['tzoffset']), unit='hours'
    )
    df_weather['date_hh'] = df_weather['datetimeEpoch'] + df_weather['tzoffset']

    return df_weather


def weather_files():
    """Generator yielding ALL csv files in data directory."""

    dir_name = os.path.join(
        str(get_project_root()),
        'data',
        'raw',
        'visualcrossing',
    )

    for fpath in sorted(os.listdir(dir_name)):
        if fpath.endswith('.yml'):
            yield os.path.join(dir_name, fpath)


import json

import oyaml as yaml


def download_visual_crossing_data(
    start_date: str, end_date: str, include_intervals: list
):
    r"""Downloads Visual Crossing Data and persists to data subdirectory.

    Args:
        start_date:
        end_date:
        include_intervals: List with legal values.
        one of 'days'|'hours'|'hours,days'.

    Returns:
        None: Persists to local json files.
    """

    for include_interval in include_intervals:

        # get filename and -path
        fname_out = weather_file_name(
            start_date, end_date, include_interval=include_interval
        )

        fpath_out = os.path.join(
            get_project_root(),
            'data',
            'raw',
            'visualcrossing',
            fname_out,
        )

        # request data
        dlurl = get_visual_crossing_request_url(
            VISUAL_CROSSING_API_KEY,
            include_interval=include_interval,
            start_date=start_date,
            end_date=end_date,
        )

        print(f'Request URL: {dlurl}')
        response = requests.get(dlurl)
        print(response.status_code)
        print(
            f"Consumed {response.json()['queryCost']} of the 1000 free queries p. day."
        )
        print(f'HTTP return code: {response.status_code}')

        print(f'Saving output file: {fpath_out}')
        save_yaml('/tmp/tmp.json', response.json())
        save_yaml(fpath_out, response.json())

        return response.json()


def get_df_all_weather_data(day_fields=None, hour_fields=None):
    """read all weather dfs into df."""

    data = []
    for fname in weather_files():
        df_weather_tmp = bikerides.utils.load_yaml(fname)
        df_tmp = get_df_weather(
            df_weather_tmp,
            day_fields=day_fields,
        )
        data.append(df_tmp)

    return pd.concat(data, axis=0)


@click.command()
@click.argument('start-date', default=None, type=click.DateTime(formats=['%Y-%m-%d']))
@click.argument('end-date', default=None, type=click.DateTime(formats=['%Y-%m-%d']))
def download_weather_data(start_date, end_date):
    """Download whether data.

    Example:
        download_weather_data 2022-09-01 2022-10-05

    """
    click.echo(f'Start: {start_date}, End: {end_date} ')
    include_intervals = ['hours,days']
    json_data = download_visual_crossing_data(
        start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'), include_intervals
    )


def ingest():

    table_name_weather = 'weather_hourly'

    DB_FNAME = 'db.sqlite3'
    DB_FPATH = os.path.join(
        str(get_project_root()),
        'data',
        'processed',
        DB_FNAME,
    )

    date_fields_hourly = ['date_hh', 'tzoffset', 'datetime', 'datetimeEpoch']
    weather_fields_hourly = [
        'temp',
        'feelslike',
        'humidity',
        'precip',
        'precipprob',
        'windspeed',
        'windgust',
        'cloudcover',
    ]
    hour_fields = date_fields_hourly + weather_fields_hourly
    df_w = get_df_all_weather_data(hour_fields=hour_fields)
    print(f'number of  hours of weather data: {df_w.shape[0]}')
    print(f'number of  days of weather data: {df_w.shape[0] / 24}')

    for field_name in weather_fields_hourly:
        df_w[field_name] = pd.to_numeric(df_w[field_name], 'coerce')
    fields_to_drop = list(set(df_w.columns) - set(hour_fields))
    df_w.drop(columns=fields_to_drop, inplace=True)

    # FIXME: unittest
    # Dropping duplicates:
    # 2013-11-21 00:00:00 has two entries for date_hh
    df_w.drop_duplicates(subset=['date_hh'], inplace=True)
    assert df_w['date_hh'].value_counts().max() == 1

    with sqlite3.connect(DB_FPATH, detect_types=sqlite3.PARSE_DECLTYPES) as conn:
        sql_result = df_w.to_sql(
            table_name_weather, conn, if_exists='replace', index=False
        )
        print(f"Result of SQL query: {sql_result}")


if __name__ == '__main__':
    # Mess with other Client
    # PROJECT_ROOT = bikerides.utils.get_project_root()
    # print(PROJECT_ROOT)
    #
    # DB_FNAME = 'db.sqlite3'
    # DB_FPATH = os.path.join(PROJECT_ROOT, 'data', 'processed', DB_FNAME)
    # from PyQt5.QtSql import QSqlDatabase
    # con = QSqlDatabase.addDatabase("QSQLITE")
    # con.setDatabaseName(DB_FPATH)
    # Open the connection
    # con.open()
    # con.isOpen()
    # breakpoint()
    # from supersqlite import sqlite3
    # conn = sqlite3.connect(DB_FPATH, detect_types=sqlite3.PARSE_DECLTYPES)

    # conn
    ...
