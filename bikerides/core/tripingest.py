import os
import sqlite3

import pandas as pd

from bikerides.utils import get_project_root


def csvfiles():
    """Generator yielding ALL csv files in raw citibike data directory."""

    dir_name = os.path.join(
        str(get_project_root()),
        'data',
        'raw',
        'citibike-tripdata',
    )

    for fpath in sorted(os.listdir(dir_name)):
        if fpath.endswith('.csv'):
            yield os.path.join(dir_name, fpath)


def remove_incorrect_tripdurations(df):
    """Remove entries where stoptime - starttime and tripduration differ.

    So far only one instance for the July dataset was seen.

    This happens in very few datasets, not time to bother.

    Args:
        df: Citibike data frame
    Returns:
        None
    """
    print(f'rows before deleting incorrect trip durations: {df.shape[0]}')
    seconds_from_datetimes = (
        (df['stoptime'] - df['starttime']).apply(lambda x: x.total_seconds()).values
    )
    mask = ((df['tripduration'] - seconds_from_datetimes) != 0).values
    df.drop(df.index[mask], inplace=True)
    print(f'rows after deleting incorrect trip durations: {df.shape[0]}')


def read_bikedides_csv_file(fpath):

    na_values = {'birth year': ['\\N']}

    df = pd.read_csv(
        fpath,
        parse_dates=['starttime', 'stoptime'],
        dtype={'usertype': 'category', 'gender': 'category'},
        na_values=na_values,
    )
    print(f'reading of {fpath} done.')

    df.pipe(remove_incorrect_tripdurations)

    return df


def ingest():

    table_name_bikes = 'bicyclerides'  # table and file name

    DB_FNAME = 'db.sqlite3'
    DB_FPATH = os.path.join(
        str(get_project_root()),
        'data',
        'processed',
        DB_FNAME,
    )

    write_mode = 'replace'
    with sqlite3.connect(DB_FPATH, detect_types=sqlite3.PARSE_DECLTYPES) as conn:
        for fpath in csvfiles():
            df = read_bikedides_csv_file(fpath)
            print(f'write {table_name_bikes} for bike rides to db {DB_FNAME}')
            result = df.to_sql(table_name_bikes, conn, if_exists=write_mode, index=False)
            write_mode = 'append'


if __name__ == '__main__':
    ...
