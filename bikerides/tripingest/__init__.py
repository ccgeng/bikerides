# Add imports here like this:
# from bikerides.core.visualcrossing import get_visual_crossing_request_url
# from bikerides.core.visualcrossing import weather_file_name

from bikerides.core.tripingest import read_bikedides_csv_file
from bikerides.core.tripingest import remove_incorrect_tripdurations
from bikerides.core.tripingest import ingest
from bikerides.core.tripingest import csvfiles

__all__ = [
    'read_bikedides_csv_file',
    'remove_incorrect_tripdurations',
    'ingest',
    'csvfiles',
]
