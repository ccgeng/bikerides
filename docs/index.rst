.. aisoundlabtools documentation master file

.. include:: ../README.rst

.. toctree::
    :caption: Submission
    :maxdepth: 2
    :hidden:

    submission

.. toctree::
    :caption: Query Development
    :maxdepth: 2
    :hidden:

    query_development

.. toctree::
    :caption: Bikeride Occurrence Prediction
    :maxdepth: 2
    :hidden:

    Bikeride_Occurrence_Prediction.nblink

.. toctree::
    :caption: Development / Contributing
    :maxdepth: 2
    :hidden:

    contributing
    changelog
