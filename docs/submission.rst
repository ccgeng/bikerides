===================
Case Study Overview
===================




1 Problem Definition
--------------------

1.1 Narrow the scope
~~~~~~~~~~~~~~~~~~~~

- Dealing with temporal information seems to be the hidden question as it gets mentioned twice: on cleaning up the data we are asked to
  pay special attention to "ensure the date/timestamp is in the right format throughout the datasets".

- The phasing is a little ambiguous: Hadoop / Horten are mentioned, but at the same time we are asked to only submit a small amount of data
  "worth a week" that does not require anything highly scalable for now.

- In the same vein wanting to ****know the weather for each entry in the main dataset**** stresses the time homogenization too.

- Scalability is not dealt with here in the first place, as this should be dealt with at the infrastructure level.
  The scope is narrowed down dealing with time information at the SQL level.
  For that purpose the database does not really matter and I use an embedded database SQLite.
  SQLite is good enough to allow to work on the topic to integrate data sources with different temporal information,
  and dialectal differences matter but the principles to negotiate time info from different sources stays the same.

1.2 Data Selection.
~~~~~~~~~~~~~~~~~~~

The first set has been the **New York City Bike Share** dataset. I simply chose it because it was available straight away.
Weather data were more difficult. After searching for quite a little while I have started to use the **VisualCrossing**.

This then has an api and with the free API key one can obtain up to 1000 requests p. day. This means that I can get a little more than
5 weeks of data a day. Against any good practice, I have committed them to the repo as they were not prohibitively big.
They can be found in ``./data/raw/`` relative to the project directory. They come in two resolutions, daily and hourly.
As bikerides are granularity-wise in the hourly ballpark, the aim is to aggregate these to an hourly scale of resolution.

1.3 Phantasy Usecase
~~~~~~~~~~~~~~~~~~~~

The queries to be developed are geared towards counting the amount of data per time interval.
This decision is made with the idea in the back of the head that being able to predict the amount of rentals by
weather conditions / forecasts might be one subtask of a system that tries to have the right supply of bikes available at
a given bike station to meet user demands.

2 Section Overview
------------------

Taking together, there will be two main sections

1. one section **Query Developement** giving details about SQL queries

2. a second sections **Bikeride Occurrence Prediction** that uses these queries and prepares data for a tentative data scientist. It will assumed that we want to build a separate model for each bike station, and we want to paramtrize a date range.


A notebook containing exploratory analysis is located under notebooks in ``Exploratory Data Analysis.ipynb`` but is not included here.
