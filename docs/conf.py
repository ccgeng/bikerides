import configparser
import os
import subprocess
from datetime import date

import audeer

config = configparser.ConfigParser()
config.read(os.path.join('..', 'setup.cfg'))

# Project -----------------------------------------------------------------
author = config['metadata']['author']
copyright = f'2022-{date.today().year} cgeng'
project = config['metadata']['name']
version = audeer.git_repo_version()
title = f'{project} Documentation'


# General -----------------------------------------------------------------
master_doc = 'index'
extensions = []
source_suffix = '.rst'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '**.ipynb_checkpoints']
pygments_style = None
extensions = [
    'nbsphinx',
    'nbsphinx_link',  # link to notebooks that are not in the source directory
    'jupyter_sphinx',  # executing code blocks
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',  # support for Google-style docstrings
    'sphinx.ext.viewcode',
    'sphinx.ext.intersphinx',
    'sphinx_autodoc_typehints',
    'sphinx_copybutton',  # for "copy to clipboard" buttons
    'sphinx_rtd_theme',
]

# Ignore package dependencies during building the docs
autodoc_mock_imports = [
    'tqdm',
]


autodoc_default_options = {
    'member-order': 'bysource',
}

# Reference with :ref:`data-header:Database`
autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 2

# Do not copy prompot output
copybutton_prompt_text = r'>>> |\.\.\. '
copybutton_prompt_is_regexp = True

# Mapping to external documentation
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
}

# Disable Gitlab if we need to sign in
# linkcheck_ignore = [
#     'https://gitlab.com',
# ]


# Disable auto-build of Jupyter notebooks
nbsphinx_execute = 'never'
# This is processed by Jinja2 and inserted before each Jupyter notebook
nbsphinx_prolog = r"""
{% set docname = env.doc2path(env.docname, base='docs') %}
{% set base_url = "https://gitlab.com/ccgeng/data-engineer-position/" %}

.. role:: raw-html(raw)
    :format: html

:raw-html:`<div class="notebook"><a href="{{ base_url }}/{{ env.config.version }}/{{ docname }}?inline=false"> Download notebook: {{ docname }}</a></div>`
"""  # noqa: E501
nbsphinx_timeout = 3600

# html_theme = 'nature'
html_theme = "sphinx_rtd_theme"

html_title = title


# -- Intersphinx ------------------------------------------------
intersphinx_mapping = {
    'numpy': ('http://docs.scipy.org/doc/numpy/', None),
    'pandas': ('https://pandas.pydata.org/pandas-docs/stable/', None),
}
