-- Open the database
.open /home/audeering.local/cgeng/work/myfiles/bikerides/data/processed/db.sqlite3
.header on
.mode csv

DROP TABLE IF EXISTS stations;

CREATE TABLE stations ("name" varchar(255),
                                    "N Rentals" INTEGER,
                                   "lat" REAL,
                                   "lon" REAL);

INSERT into stations (name, "N Rentals", lat, lon)
select "start station name" as name,
       count(*) as "N Rentals",
       "start station latitude" as lat,
       "start station longitude" as lon
from vw_bicyclerides
GROUP BY "start station name"
ORDER BY "start station name" ASC;

DROP TABLE IF EXISTS stations;

CREATE TABLE stations ("name" varchar(255),
                                    "N Rentals" INTEGER,
                                   "lat" REAL,
                                   "lon" REAL);

INSERT into stations (name, "N Rentals", lat, lon)
select "start station name" as name,
       count(*) as "N Rentals",
       "start station latitude" as lat,
       "start station longitude" as lon
from vw_bicyclerides
GROUP BY "start station name"
ORDER BY "start station name" ASC;

-- (1) Define Station name variable to be used in views
-- A station name that can be used in views

DROP TABLE IF EXISTS BIKE_STATION;
CREATE TABLE IF NOT EXISTS BIKE_STATION AS SELECT "York St & Jay St" AS _name;
-- UPDATE BIKE_STATION SET _name = 'West Thames St';
-- UPDATE BIKE_STATION SET _name = 'West St & Chambers St';
UPDATE BIKE_STATION SET _name = 'Pershing Square N';
-- UPDATE BIKE_STATION SET _name = 'Willoughby Ave & Hall St';
-- Use with:
-- SELECT _name FROM BIKE_STATION;


-- (2) Define Start Time to be used in views
DROP TABLE IF EXISTS START_TIME;

CREATE TABLE IF NOT EXISTS START_TIME AS
select _value
from
  (select min(date_hh) as _value
   from vw_bicyclerides WHERE "start station name" = (SELECT _name FROM BIKE_STATION)) as _value;
-- usage: select _value from START_TIME;
-- set value: UPDATE START_TIME SET _value = datetime('2013-07-21');

-- (3) Define End Time to be used in views, initialize as latest timepoint of station
DROP TABLE IF EXISTS END_TIME;

CREATE TABLE IF NOT EXISTS END_TIME AS
select _value
from
  (select max(date_hh) as _value
   from vw_bicyclerides WHERE "start station name" = (SELECT _name FROM BIKE_STATION)) as _value;
-- usage: select _value from END_TIME;
-- set value: UPDATE END_TIME SET _value = datetime('2013-07-31');

DROP VIEW IF EXISTS vw_bicyclerides;

CREATE VIEW vw_bicyclerides AS
SELECT *
FROM
  (SELECT *,
          datetime(strftime('%Y-%m-%d %H:00:00', datetime(half_time_sql, '+30 minutes'))) as date_hh,
          date(half_time_sql) as date_day
   FROM
     (SELECT *,
             datetime(strftime('%s', substr(stoptime, 1, 19)) - 0.5 * tripduration, 'unixepoch') as half_time_sql
      FROM bicyclerides) x) y;

-- ORDERING BAD??  ORDER BY "start station name", date_hh
-- DROP INDEX IF EXISTS idx_station; ??
-- CREATE INDEX idx_station ON bicyclerides ("start station name", starttime);

DROP VIEW IF EXISTS vw_station_ridecounts;

CREATE VIEW vw_station_ridecounts AS
select date_hh as date_hh,
       COUNT(*) as N,
       AVG(tripduration) as "tripduration[m]",
       "start station name"
from vw_bicyclerides
WHERE "start station name" = (SELECT _name FROM BIKE_STATION)
GROUP BY date_hh;

-- All
DROP VIEW if EXISTS vw_bicyclerides_S;
CREATE VIEW vw_bicyclerides_S AS
select * from vw_bicyclerides
WHERE "start station name" = (SELECT _name FROM BIKE_STATION)
AND date_hh BETWEEN (select _value from START_TIME) AND (select _value from END_TIME)
limit (select count(*) from vw_bicyclerides);

-- Debug: Limit data to some number
DROP VIEW if EXISTS vw_bicyclerides_S;
CREATE VIEW vw_bicyclerides_S AS
select * from vw_bicyclerides
WHERE "start station name" = (SELECT _name FROM BIKE_STATION)
AND date_hh BETWEEN (select _value from START_TIME) AND (select _value from END_TIME)
limit 5562202;


DROP VIEW IF EXISTS vw_station_ridecounts_windowed;

CREATE VIEW vw_station_ridecounts_windowed AS
WITH RECURSIVE date_times(datetime_start) AS (
  VALUES((SELECT _value FROM START_TIME))
  UNION ALL
  SELECT datetime(datetime_start, '+60 minutes')
  FROM date_times
  WHERE datetime_start < (SELECT _value FROM END_TIME)
)
SELECT
bikes."start station name",
dt.datetime_start,
datetime(dt.datetime_start, '+240 minutes') as date_hh_end,
count (*) N,
strftime('%w', bikes.date_hh) as 'week_day',
strftime('%H', bikes.date_hh) as 'hour_of_day'
FROM date_times dt inner join (select * from vw_bicyclerides_S WHERE "start station name" = (SELECT _name FROM BIKE_STATION)) as bikes
ON bikes.date_hh >= dt.datetime_start AND bikes.date_hh < datetime(dt.datetime_start, '+240 minutes')
GROUP BY dt.datetime_start, datetime(dt.datetime_start, '+240 minutes');
