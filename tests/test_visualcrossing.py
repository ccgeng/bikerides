import os

import pandas as pd
import pytest
from pandas.tseries.frequencies import build_field_sarray

import bikerides.utils
import bikerides.visualcrossing
from bikerides.utils import get_project_root
from bikerides.visualcrossing import weather_files


def test_format_url():
    """test basic formatting of urls"""
    url = bikerides.visualcrossing.get_visual_crossing_request_url(
        os.getenv('VISUAL_CROSSING_API_KEY'), include_interval='hours,days'
    )

    result_gold_standard = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/New%20York/2013-07-01/2014-02-28?unitGroup=metric&include=hours%2Cdays&key=6JUD3B5J4ZP469QBHXCZESTKA&contentType=json'  # noqa: E501
    assert url == result_gold_standard, 'some mismatch'


@pytest.mark.skip(reason=None)
def test_get_df_weather_parametrized():
    """Happy flow test on reading csvs specifying custom day and month fields."""

    weather_file_names = [x for x in weather_files()]
    test_data = bikerides.utils.load_yaml(weather_file_names[0])

    day_fields = ['datetime', 'datetimeEpoch']  # , 'tempmax','tempmin'
    hour_fields = ['datetime', 'datetimeEpoch']  # 'temp'
    # colnames = [x + '[d]' for x in day_fields] + [x + '[h]' for x in hour_fields]

    df = bikerides.visualcrossing.get_df_weather(
        test_data,
        day_fields=day_fields,
        hour_fields=hour_fields,
    )

    assert isinstance(df, pd.DataFrame)


def test_get_df_weather_unparametrized():
    """Happy flow test on reading csvs specifying."""
    weather_file_names = [x for x in weather_files()]
    test_data = bikerides.utils.load_yaml(weather_file_names[0])

    df = bikerides.visualcrossing.get_df_weather(
        test_data,
    )

    assert isinstance(df, pd.DataFrame)


def test_get_df_all_weather_data():
    """Happy Flow to test all weather data."""
    bikerides.visualcrossing.get_df_all_weather_data()
