import pytest
import bikerides
import os
import pandas as pd
from bikerides.tripingest import read_bikedides_csv_file
from bikerides.tripingest import csvfiles

# azure data factory


@pytest.mark.skip(reason=None)
@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_over_files(filename):
    df = read_bikedides_csv_file(filename)
    assert df['usertype'].value_counts().sum() == df.shape[0]


@pytest.mark.skip(reason=None)
@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_start_dates_relate_to_month(filename):
    """Test that month inferred from csv file name coheres with date from data."""
    # df, fpath = non_empty_file

    df = read_bikedides_csv_file(filename)
    csv_date = pd.to_datetime(os.path.basename(filename.split()[0]))

    assert all(
        df['starttime'].dt.month.values == csv_date.month
    ), 'month not parsed correctly at least once'


@pytest.mark.skip(reason=None)
@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_start_dates_relate_to_year(filename):
    """Test that year inferred from csv file name coheres with date from data."""

    df = read_bikedides_csv_file(filename)
    csv_date = pd.to_datetime(os.path.basename(filename.split()[0]))

    assert all(
        df['starttime'].dt.year.values == csv_date.year
    ), ' at least one inconsistencey between fname and csv year.'


@pytest.mark.skip(reason=None)
@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_trip_durations(filename):
    """Test that all calculated trip durations match with stoptime/starttime diffs."""
    df = read_bikedides_csv_file(filename)
    seconds_from_datetimes = (
        (df['stoptime'] - df['starttime']).apply(lambda x: x.total_seconds()).values
    )
    mask = ((df['tripduration'] - seconds_from_datetimes) == 0).values
    assert all(mask), 'There are instances of trip duration mismatch with timestamps'


@pytest.mark.skip(reason=None)
@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_start_station_names(filename):
    """Test that all calculated trip durations match with stoptime/starttime diffs."""

    df = read_bikedides_csv_file(filename)
    # df.sort_values(by=['start station id'], inplace=True)
    dd = pd.crosstab(df['start station id'], df['start station name']).values
    assert (dd > 0).sum().sum() == dd.shape[0]


@pytest.mark.parametrize('filename', bikerides.tripingest.csvfiles())
def test_end_station_names(filename):
    """Test that all calculated trip durations match with stoptime/starttime diffs."""

    df = read_bikedides_csv_file(filename)
    # df.sort_values(by=['start station id'], inplace=True)
    dd = pd.crosstab(df['end station id'], df['end station name']).values
    assert (dd > 0).sum().sum() == dd.shape[0]


# Indirect Parametrization also reads csvs for every test
# @pytest.mark.skip(reason=None)
# @pytest.mark.parametrize(
#     'non_empty_file',
#     csvfiles(),
#     indirect=True,
# )
# def test_that_usertype_has_no_missing_values(non_empty_file):
#     """Assert that usertype is always there - no nan treatment required.

#     Test works because pandas dropna is the default,

#     Once we know that we can safely convert it to categorical
#     """
#     df, _ = non_empty_file
#     assert df.usertype.value_counts().sum() == df.shape[0]

# def test_start_station_names():
#     """Test that all calculated trip durations match with stoptime/starttime diffs."""
#     breakpoint()
# df, fpath = non_empty_file
# df.sort_values(by=['start station id'], inplace=True)
# dd = pd.crosstab(df['start station id'], df['start station name']).values
# assert (dd > 0).sum().sum() == dd.shape[0]

# One could calculate Levenshtein distances Brute Force
# 329 * 329
# words = ['hello', 'Hallo', 'hi', 'house', 'key', 'screen', 'hallo', 'question', 'format']
# difflib.get_close_matches('Hello', words)
# ['hello', 'Hallo', 'hallo']
# https://stackoverflow.com/questions/10018679/python-find-closest-string-from-a-list-to-another-string
# df['start station name'].value_counts(dropna=False).index
