============
Contributing
============




1 Development Installation
--------------------------

Instead of pip-installing the latest release from PyPI,
you should get the newest development version from Github from `https://gitlab.com/ccgeng/data-engineer-position <https://gitlab.com/ccgeng/data-engineer-position>`_


::

    git clone git@gitlab.com:ccgeng/data-engineer-position.git bikerides
    cd bikerides
    # Create virutal environment for this project
    # e.g.
    # virtualenv --python="python3"  $HOME/.envs/audformat
    # source $HOME/.envs/audformat/bin/activate
    pip install -r requirements.txt


This way, your installation always stays up-to-date,
even if you pull new changes from the Github repository.

This will install the code as a editable package, but also install some development requirements.
It these are not required, ``pip install -e .`` is the way to go.

2 Usage
-------

2.1 Obtaining data
~~~~~~~~~~~~~~~~~~

Data download is not automatized yet, and needs to be done by hand.
An example shell script might look like:

::

    (
    cd ./data/raw/citibike-tripdata
    wget https://s3.amazonaws.com/tripdata/201307-201402-citibike-tripdata.zip
    unzip 201307-201402-citibike-tripdata.zip
    )

Your data directory can contain the csvs and zips alike. Mine looks like this:

.. code:: bash

    ls ./data/raw/citibike-tripdata -l

2.2 Processing data
~~~~~~~~~~~~~~~~~~~

``setup.cfg`` details the command line tools that can be used to get data into an sqlite database.

::

       ingest_bike_rides=bikerides.tripingest:ingest

    [tool:pytest]
    python_files = test_*.py
    addopts =



**download\_weather\_data** will download more weather data. The API key is currently hardcoded into the module :-(


**ingest\_visualcrossing** ingests the weather data in the database in ``./data/processed/db.sqlite3``

**ingest\_bike\_rides** is a poor man's bikeride ingestion.


.. code:: bash

    download_weather_data

::

    : command not found


download\_weather\_data
